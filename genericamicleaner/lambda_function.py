#!/usr/bin/python3

"""
Cleans 90 days old unused AMIs and their unused snapshots
"""

import os
import boto3
import re
from datetime import datetime, timedelta

def lambda_handler(event, context):

    # Get Account Id from lambda function arn using passed in context
    print("lambda arn: " + context.invoked_function_arn)
    # Get Account ID from lambda function arn in the context
    ACCOUNT_ID = context.invoked_function_arn.split(":")[4]
    print("Account ID=" + ACCOUNT_ID)

    ec2 = boto3.resource("ec2")

    # Gather AMIs and figure out which ones to delete
    my_images = ec2.images.filter(Owners=[ACCOUNT_ID])

    # Let's figure out which images to spare
    # Don't delete images in use
    used_images = {
        instance.image_id for instance in ec2.instances.all()
    }

    # Keep everything younger than four weeks
    young_images = set()
    for image in my_images:
        created_at = datetime.strptime(
            image.creation_date,
            "%Y-%m-%dT%H:%M:%S.000Z",
        )
        if created_at > datetime.now() - timedelta(90):
            young_images.add(image.id)

    # Keep the latest one if there are multiple images with the same name
    # Comment in if needed
    # latest = dict()
    # for image in my_images:
    #     split = image.name.split('-')
    #     try:
    #         timestamp = int(split[-1])
    #     except ValueError:
    #         continue
    #     name = '-'.join(split[:-1])
    #     if(
    #             name not in latest
    #             or timestamp > latest[name][0]
    #     ):
    #         latest[name] = (timestamp, image)
    # latest_images = {image.id for (_, image) in latest.values()}

    # Create a safe var
    safe = used_images | young_images # | latest_images

    # Create a deleted_images list
    deleted_images = list()

    # Delete everything that is not in safe
    for image in (
        image for image in my_images if image.id not in safe
    ):
        print('Deregistering {} ({})'.format(image.name, image.id))
        # add to deleted_images list
        deleted_images.append(image.id)
        image.deregister()

    print("Deleted images:" + str(deleted_images))

    # Delete the now unattached snapshots
    print('Deleting snapshots.')
    for snapshot in ec2.snapshots.filter(OwnerIds=[ACCOUNT_ID]):
        print('Checking {}'.format(snapshot.id))
        r = re.match(r".*for (ami-.*) from.*", snapshot.description)
        if r:
            if r.groups()[0] in deleted_images:
                print('Deleting {}'.format(snapshot.id))
                snapshot.delete()
