#!/bin/bash

# install sam ofc
sam validate
echo "Testing the ${PRODUCT} with SAM by invoking it with ${EVENT}"
echo "Making sure lambda files are executable..."
chmod -R 777 .
echo "Running local invoke with SAM using stdin as event..."
echo "Make sure docker engine is running and proxy aware..."
EVENT={}
# sam local invoke accepts stdin as an event too. Alternatively you could pass in an event with -e
# for example: echo '{"message": "Hey, are you there?" }' or for a cloudwatch trigger: echo '{}'
$EVENT  | sudo -E sam local invoke
sam package --template-file template.yml --s3-bucket yours3bucket --output-template-file packaged.yaml
echo "Sleep 5 seconds to let S3 eventual consistency catch up... "
sleep 5
echo "Deploying product with SAM..."
PARAMETER_OVERRIDES= 
sam deploy --template-file packaged.yaml --stack-name somethingyoulike --capabilities CAPABILITY_IAM $PARAMETER_OVERRIDES